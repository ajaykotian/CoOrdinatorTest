package com.example.ajaykotian.coordinatortest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;

import static android.R.attr.bitmap;
import static android.R.attr.colorPrimary;

public class MainActivity extends AppCompatActivity {


    protected CollapsingToolbarLayout collapseToolbar;
    protected ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        collapseToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapse_Toolbar);
        imageView = (ImageView) findViewById(R.id.iv_main);

        new ImageLoad().execute();
    }


    protected class ImageLoad extends AsyncTask<Void,Void,Void> {


        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.material_flat);

        @Override
        protected Void doInBackground(Void... params) {

            URL url = null;

            try {
                url = new URL("https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/2000px-Google_2015_logo.svg.png");

                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }


            Palette.generateAsync(bitmap,
                    new Palette.PaletteAsyncListener() {
                        @Override
                        public void onGenerated(Palette palette) {
                            Palette.Swatch vibrant = palette.getVibrantSwatch();

                            if (vibrant != null) {
                                // If we have a vibrant color
                                // update the title TextView
                                collapseToolbar.setBackgroundColor(vibrant.getRgb());
                                //  mutedColor = palette.getMutedColor(R.attr.colorPrimary);
                                collapseToolbar.setStatusBarScrimColor(palette.getDarkMutedColor(vibrant.getRgb()));
                                collapseToolbar.setContentScrimColor(palette.getMutedColor(vibrant.getRgb()));

                            }
                        }
                    });

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            imageView.setImageBitmap(bitmap);
        }
    }
}
